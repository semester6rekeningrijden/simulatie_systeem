package utils;

import com.google.gson.Gson;
import models.Config;

import java.io.*;

public class GsonHelper {
    /**
     * Save config to JSON
     */
    public static void saveConfig(Config config, String configLocation) throws IOException {
        Writer writer =  new FileWriter(configLocation);
        try{
            Gson gson = new Gson();
            gson.toJson(config, writer);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            writer.flush();
            writer.close();
        }
    }

    /**
     * Load config from JSON
     */
    public static Config loadConfig(String configLocation) throws IOException {
        Reader reader = new FileReader(configLocation);
        try{
            Gson gson = new Gson();
            return gson.fromJson(reader, Config.class);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            reader.close();
        }
        return new Config();
    }
}
