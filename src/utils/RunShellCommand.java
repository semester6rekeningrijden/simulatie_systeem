package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunShellCommand implements Runnable {
    private String cmd;
    private static boolean running = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(RunShellCommand.class);

    public static void terminate() {
        running = false;
    }

    public RunShellCommand(String cmd) {
        this.cmd = cmd;
    }

    public static void Start(String cmd) {
        String s;
        int increment = 100;

        try {
            Process p = Runtime.getRuntime().exec(cmd);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            // read the output from the command
            System.out.println("Output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                //print increments to prevent lag
                if (s.contains("Step")) {
                    if (s.contains("#" + increment)) {
                        System.out.println(s);
                        increment += 100;
                    }
                } else {
                    System.out.println(s);
                }
            }

            // read any errors from the attempted command
            System.out.println("\nErrors of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            System.out.println("\n Exception happened: ");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (running) {
            Start(this.cmd);
        }
    }
}
