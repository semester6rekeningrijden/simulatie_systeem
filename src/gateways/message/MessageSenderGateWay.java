package gateways.message;

import javax.jms.*;

public class MessageSenderGateWay {

    private Destination destination;
    private MessageProducer producer;
    private MessageConnectionGateway connectionGateway;

    public MessageSenderGateWay(String channelName) {
        setup(channelName);
    }

    private void setup(String channelName) {
        try {
            this.connectionGateway = new MessageConnectionGateway();
            this.connectionGateway.getConnection().start();
            this.destination = connectionGateway.getSession().createQueue(channelName);
            this.producer = connectionGateway.getSession().createProducer(destination);
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    public Message createTextMessage(String body) {
        try {
            return this.connectionGateway.getSession().createTextMessage(body);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendMessage(Message msg) {
        try {
            this.producer.send(msg);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
