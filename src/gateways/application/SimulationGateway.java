package gateways.application;

import gateways.message.MessageSenderGateWay;
import models.JourneyData;
import serialization.JourneyDataSerializer;

import javax.jms.Message;

public class SimulationGateway {
    private MessageSenderGateWay sender;

    private JourneyDataSerializer journeyDataSerializer;

    public SimulationGateway() {
        this.journeyDataSerializer = new JourneyDataSerializer();
        this.sender = new MessageSenderGateWay("journeyDataSender");
    }

    /**
     * Send JourneyData with JMS to registration system
     * @param journeyData Car position with car and position data
     */
    public void sendData(JourneyData journeyData){
        Message message = this.sender.createTextMessage(journeyDataSerializer.toString(journeyData));
        sender.sendMessage(message);
    }
}
