package controllers;

import gateways.application.SimulationGateway;
import models.Config;
import models.JourneyData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import utils.GsonHelper;
import utils.RunShellCommand;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class SimulationController {

    public static List<JourneyData> journeyDataList;

    private SimulationGateway gateway;
    private RunShellCommand runnable;
    private Thread t;

    public SimulationController() {
        gateway = new SimulationGateway();

        // Start simulation
        try {
            Config config = GsonHelper.loadConfig("config.json");
            startSimulation(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the simulation with chosen settings.
     * @param config Settings for simulation
     * @throws IOException Exceptions for file reading
     */
    public void startSimulation(Config config) throws IOException {
        // Run shell command
        String cmd = "sumo -c \"" + config.getINPUT_LOCATION() + "\" --fcd-output localhost:" +
                config.getSERVER_SOCKET() + " --fcd-output.geo --end " +
                config.getDURATION();

        t = new Thread(runnable = new RunShellCommand(cmd));
        t.start();

        // Read output through Socket
        ServerSocket serverSocket = new ServerSocket(config.getSERVER_SOCKET());
        Socket clientSocket = serverSocket.accept();
        InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream());
        BufferedReader br = new BufferedReader(isr);
        runnable.terminate();

        String s;
        StringBuilder stringBuilder = new StringBuilder();
        while ((s = br.readLine()) != null) {
            StringBuffer sb = new StringBuffer(s);
            stringBuilder.append(sb);
        }
        isr.close();

        // Write information to models
        readXML(stringBuilder.toString());

    }

    /**
     * Read XML and fill lists with data
     * @param xml file location
     */
    private void readXML(String xml) {
        try {
            journeyDataList = new ArrayList<>();

            Document doc = convertStringToXMLDocument(xml);
            assert doc != null;
            doc.getDocumentElement().normalize();

            NodeList nListTimestep = doc.getElementsByTagName("timestep");
            System.out.println("Seconds simulated: " + nListTimestep.getLength());
            NodeList nListVehicle = doc.getElementsByTagName("vehicle");
            System.out.println("Number of journeydata added: " + nListVehicle.getLength()*nListTimestep.getLength());

            // timestep
            for (int i = 0; i < nListTimestep.getLength(); i++) {
                JourneyData journeyData = null;

                Node nNodeTimestep = nListTimestep.item(i);
                String time = "0"; // time

                if (nNodeTimestep.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNodeTimestep;
                    time = eElement.getAttribute("time"); // time
                }
                boolean finished = false;
                int check = 0;

                // vehicle
                for (int temp = 0; temp < nListVehicle.getLength(); temp++) {
                    Node nNode = nListVehicle.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String id = eElement.getAttribute("id"); // tracker_id
                        String longitude = eElement.getAttribute("x"); // longitude
                        String latitude = eElement.getAttribute("y"); // latitude
                        String vehicleType = eElement.getAttribute("type"); // vehicle_type
                        journeyData = new JourneyData(time, latitude, longitude, vehicleType, id);
                        // if journey active add journey data else create list and start recording journeydata
                        int time2 = Integer.parseInt(time.substring(0, time.length()-3))+1;
                        if(nListTimestep.getLength() == time2 && check == Integer.parseInt(id)) {
                            finished = true;
                            check++;
                        } else {
                            finished = false;
                        }

                        if(finished == true) {
                            System.out.println("finished");
                        }
                        journeyData.setFinished(finished);
                    }

                    this.gateway.sendData(journeyData);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }

    /**
     * Function for interpreting string output as xml
     * @param xmlString line from XML
     * @return
     */
    private Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            return builder.parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
