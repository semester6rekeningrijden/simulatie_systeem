package controllers;

import org.springframework.cglib.core.Local;
import utils.RunShellCommand;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;

/**
 * Class for easy generation of trips
 * Note: Python has to be installed on your computer
 */
public class GenerateTrips {

    private final static String VEHICLE_AMOUNT = "2";
    private final static String INPUT_NET = "sumo/paris.net.xml";
    private final static String OUTPUT_ROU = "sumo/paris.rou.xml";

    public static void main(String[] args) {
        LocalDateTime start = LocalDateTime.now();
        System.out.println("Starting time: " + start);

        String cmd = "py sumo/randomTrips.py -n " + INPUT_NET + " -r " + OUTPUT_ROU + " -e " + VEHICLE_AMOUNT + " -l ";

        RunShellCommand.Start(cmd);

        // Parsing time required
        Duration duration = Duration.between(start, LocalDateTime.now());
        long diff = Math.abs(duration.getSeconds());
        long diffMin = Math.abs(duration.toMinutes());
        System.out.println("\n==================\nTotal seconds required: " + diff + "\nTotal minutes required: " + diffMin);
    }
}
