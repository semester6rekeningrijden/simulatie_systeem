package serialization;

import models.JourneyData;

public class JourneyDataSerializer extends ObjectSerializer<JourneyData> {
    @Override
    protected Class<JourneyData> getDomainClass() {
        return JourneyData.class;
    }
}
