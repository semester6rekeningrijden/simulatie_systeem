package serialization;

import models.Config;

public class ConfigSerializer extends ObjectSerializer<Config> {
    @Override
    protected Class<Config> getDomainClass() {
        return Config.class;
    }
}
