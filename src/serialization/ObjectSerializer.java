package serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class ObjectSerializer<T> {
    private Gson gson;

    abstract protected Class<T> getDomainClass();

    public ObjectSerializer() {
        this.gson = new GsonBuilder().create();
    }

    public String toString(T object) {
        return gson.toJson(object);
    }

    public T fromString(String str) {
        return gson.fromJson(str, getDomainClass());
    }
}
