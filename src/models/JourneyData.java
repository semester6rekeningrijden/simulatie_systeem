package models;

import org.springframework.cglib.core.Local;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class JourneyData implements Serializable {
    private LocalDateTime dateTime;
    private Timestamp test;
    private double latitude;
    private double longitude;
    private double angle;
    private double speed;
    private int tracker_id;
    private String vehicleType;
    private boolean finished;

    public JourneyData(LocalDateTime dateTime, double latitude, double longitude, double angle, double speed, int tracker_id, String vehicleType) {
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.angle = angle;
        this.speed = speed;
        this.tracker_id = tracker_id;
        this.vehicleType = vehicleType;
    }

    public JourneyData(double latitude, double longitude, double angle, double speed) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.angle = angle;
        this.speed = speed;
        this.dateTime = LocalDateTime.now();
    }

    public JourneyData(String time, String latitude, String longitude, String speed, String vehicleType, String tracker_id){
        double dd = Double.parseDouble(time);
        LocalDateTime d = LocalDateTime.now().plusSeconds((long) dd);
        this.test = Timestamp.valueOf(d);
        this.latitude = Double.parseDouble(latitude);
        this.longitude = Double.parseDouble(longitude);
        this.speed = Double.parseDouble(speed);
        this.vehicleType = vehicleType;
        this.tracker_id = Integer.parseInt(tracker_id);
    }

    public JourneyData(String time, String latitude, String longitude, String vehicleType, String tracker_id){
        double dd = Double.parseDouble(time);
        LocalDateTime d = LocalDateTime.now().plusSeconds((long) dd);
        this.dateTime = d;
        this.latitude = Double.parseDouble(latitude);
        this.longitude = Double.parseDouble(longitude);
        this.vehicleType = vehicleType;
        this.tracker_id = Integer.parseInt(tracker_id) + 1;
        this.finished = false;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(int tracker_id) {
        this.tracker_id = tracker_id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public boolean getFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }


}
