package models;

import java.util.ArrayList;
import java.util.List;

public class Journey {
    private int tracker_id;
    private String vehicleType;

    private List<JourneyData> journeyData;

    public Journey(int tracker_id, String vehicleType) {
        this.tracker_id = tracker_id;
        this.vehicleType = vehicleType;
        journeyData = new ArrayList<>();
    }

    public Journey(int tracker_id, String vehicleType, List<JourneyData> journeyData) {
        this.tracker_id = tracker_id;
        this.vehicleType = vehicleType;
        this.journeyData = journeyData;
    }

    public Journey(String id, String vehicleType) {
        this.tracker_id = Integer.parseInt(id);
        this.vehicleType = vehicleType;
        journeyData = new ArrayList<>();
    }

    public void addJourneyData(JourneyData journeyData) {
        this.journeyData.add(journeyData);
    }

    public List<JourneyData> getJourneyData() {
        return journeyData;
    }

    public void setJourneyData(List<JourneyData> journeyData) {
        this.journeyData = journeyData;
    }

    public int getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(int tracker_id) {
        this.tracker_id = tracker_id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

}
