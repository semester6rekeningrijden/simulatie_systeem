package models;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Config implements Serializable {
    private int SERVER_SOCKET;
    private String DURATION;
    private String DUMP_LOCATION;
    private String OUTPUT_LOCATION;
    private String INPUT_LOCATION;
    private List<String> VEHICLE_TYPES;
    private String INITIAL_CONTEXT_FACTORY;
    private String PROVIDER_URL;

    public Config(){
        VEHICLE_TYPES = new ArrayList<>();
        VEHICLE_TYPES.add("CAR");
        VEHICLE_TYPES.add("TRUCK");
        VEHICLE_TYPES.add("BUS");

        SERVER_SOCKET = 12345;
        DUMP_LOCATION = "sumo/paris_dump.xml";
        OUTPUT_LOCATION = "sumo/paris_output.xml";
        INPUT_LOCATION = "sumo/paris.sumocfg";
        PROVIDER_URL = "tcp://localhost:61616";
        INITIAL_CONTEXT_FACTORY = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
    }

    public int getSERVER_SOCKET() {
        return SERVER_SOCKET;
    }

    public void setSERVER_SOCKET(int SERVER_SOCKET) {
        this.SERVER_SOCKET = SERVER_SOCKET;
    }

    public String getDURATION() {
        return DURATION;
    }

    public void setDURATION(String DURATION) {
        this.DURATION = DURATION;
    }

    public String getDUMP_LOCATION() {
        return DUMP_LOCATION;
    }

    public void setDUMP_LOCATION(String DUMP_LOCATION) {
        this.DUMP_LOCATION = DUMP_LOCATION;
    }

    public String getOUTPUT_LOCATION() {
        return OUTPUT_LOCATION;
    }

    public void setOUTPUT_LOCATION(String OUTPUT_LOCATION) {
        this.OUTPUT_LOCATION = OUTPUT_LOCATION;
    }

    public List<String> getVEHICLE_TYPES() {
        return VEHICLE_TYPES;
    }

    public void setVEHICLE_TYPES(List<String> VEHICLE_TYPES) {
        this.VEHICLE_TYPES = VEHICLE_TYPES;
    }

    public String getINITIAL_CONTEXT_FACTORY() {
        return INITIAL_CONTEXT_FACTORY;
    }

    public void setINITIAL_CONTEXT_FACTORY(String INITIAL_CONTEXT_FACTORY) {
        this.INITIAL_CONTEXT_FACTORY = INITIAL_CONTEXT_FACTORY;
    }

    public String getPROVIDER_URL() {
        return PROVIDER_URL;
    }

    public void setPROVIDER_URL(String PROVIDER_URL) {
        this.PROVIDER_URL = PROVIDER_URL;
    }

    public String getINPUT_LOCATION() {
        return INPUT_LOCATION;
    }

    public void setINPUT_LOCATION(String INPUT_LOCATION) {
        this.INPUT_LOCATION = INPUT_LOCATION;
    }

    @Override
    public String toString() {
        return "Config{" +
                "SERVER_SOCKET='" + SERVER_SOCKET + '\'' +
                ", DURATION='" + DURATION + '\'' +
                ", DUMP_LOCATION='" + DUMP_LOCATION + '\'' +
                ", OUTPUT_LOCATION='" + OUTPUT_LOCATION + '\'' +
                ", INPUT_LOCATION='" + INPUT_LOCATION + '\'' +
                ", VEHICLE_TYPES=" + VEHICLE_TYPES + '\'' +
                ", PROVIDER_URL=" + PROVIDER_URL + '\'' +
                ", INITIAL_CONTEXT_FACTORY=" + INITIAL_CONTEXT_FACTORY +
                '}';
    }

}
